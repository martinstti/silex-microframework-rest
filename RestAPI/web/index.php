<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
ini_set('display_errors', 0);
require_once __DIR__.'/../vendor/autoload.php';

$app = require __DIR__.'/../src/app.php';
require __DIR__.'/../config/prod.php';
require __DIR__.'/../src/controllers.php';

$app->get("/show/user", function() use ($app){
    $entity_manager = $app["orm.em"];
    $users = $entity_manager->getRepository('Entity\User')->findAll();

    return $app['twig']->render('users.html.twig',array("users"=>$users));

});

$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});
$app->post('/new', function (Request $request) use ($app) {

    $user = new \Entity\User();
    $email = $request->request->get('email');
    $password = $request->request->get('password');
    $user->setEmail($email);
    $user->setPassword($password);
    $entity_manager = $app["orm.em"];
    $entity_manager->persist($user);
    $entity_manager->flush();

    return new Response('Thank you!', 201);

});



$app->run();
