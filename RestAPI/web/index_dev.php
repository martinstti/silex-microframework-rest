<?php

use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ParameterBag;

// This check prevents access to debug front controllers that are deployed by accident to production servers.
// Feel free to remove this, extend it, or make something more sophisticated.
if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || !in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', 'fe80::1', '::1'))
) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
}

require_once __DIR__.'/../vendor/autoload.php';

Debug::enable();
$app = require __DIR__.'/../src/app.php';
require __DIR__.'/../config/dev.php';
require __DIR__.'/../src/controllers.php';


$app->get('/hello', function () {
    return 'Hello!';
});
$blogPosts = array(
    1 => array(
        'date'   => '2011-03-29',
        'author' => 'igorw',
        'title'  => 'Using Silex',
        'body'   => '...',
    ),
);
$app->get('/libros/{id}', function ($id) use ($app,$blogPosts) {

    $libro = $blogPosts[$id];
    if (!$libro) {
        $error = array('message' => 'No se ha encontrado el libro.');

        return $app->json($error, 404);
    }
    return $app->json($libro);
});


$app->get("/new/user", function() use ($app){
    $user = new \Entity\User();
    $user->setEmail("example@simettric.com");
    $user->setPassword("lalala");
    $entity_manager = $app["orm.em"];
    $entity_manager->persist($user);
    $entity_manager->flush();
    $output = '<br />';
    return $output;
});

$app->get("/show/user", function() use ($app){
    $entity_manager = $app["orm.em"];
    $users = $entity_manager->getRepository('Entity\User')->findAll();

    return $app['twig']->render('users.html.twig',array("users"=>$users));

});

$app->get("/show/json/user", function() use ($app){
    $entity_manager = $app["orm.em"];
    $users = $entity_manager->getRepository('Entity\User')->findAll();
    foreach($users as $user){
        $usuarios['Codigos'][] = $user->getId();
        $usuarios['Correos'][] = $user->getEmail();
    }

    return $app->json($usuarios);

});


$app->get("/show/json/user/{id}", function($id) use ($app){
    $entity_manager = $app["orm.em"];
    $users = $entity_manager->getRepository('Entity\User')->findById($id);
    foreach($users as $user){
        $correo[$id][] = $user->getEmail();

    }
    return $app->json($correo);
});

/*-------------------------------POST-------------------------------------------*/

$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});
$app->post('/blog/posts', function (Request $request) use ($app) {
    $post = array(
        'title' => $request->request->get('title'),
        'body'  => $request->request->get('body'),
    );

    $post['id'] = createPost($post);

    return $app->json($post, 201);
});
$app->run();
